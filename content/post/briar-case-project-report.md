---
title: "Briar Case Project Report"
date: 2021-09-01T09:04:57+02:00
image: "/assets/introduction-preview.png"
toc: false
draft: false
---

We have completed the initial development phase for two features in Briar building using Dark Crystal, and conducted two user testing sessions for each feature.

Here is our report which covers both using the developer toolkit and findings from the user testing sessions:

- [Report on the social backup and remote wipe features](/assets/briar-case-project-report.pdf)
