---
title: "Dark Crystal Key Backup Javascript API Documentation Index"
---

- [Key Backup](https://gitlab.com/dark-crystal-javascript/key-backup) - Social Key Backup and Recovery
- [Secret-sharing](https://gitlab.com/dark-crystal-javascript/secret-sharing) - Secret Sharing using [dsprenkles/sss](https://github.com/dsprenkels/sss)
- [Key Backup Crypto](https://gitlab.com/dark-crystal-javascript/key-backup-crypto) - Crypto operations and other functionality for Dark Crystal key backup
- [Key Backup Message Schemas](https://gitlab.com/dark-crystal-javascript/key-backup-message-schemas) - Template message schemas for Dark Crystal key backup and recovery
- [Ephemeral keys](https://gitlab.com/dark-crystal-javascript/ephemeral-keys) - Generate, store, use and remove single-use keypairs for encryption.
- [Confirm Key](https://gitlab.com/dark-crystal-javascript/confirm-key) - Represent a portion of a key as a short mnemonic to aid in verbally confirming two keys are the same
- [Paper Keys](https://gitlab.com/dark-crystal-javascript/paper-keys) - Export cryptographic keys into an easily printable format

