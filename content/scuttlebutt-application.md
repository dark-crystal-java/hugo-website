---
title: "Dark Crystal Key Recovery on Secure Scuttlebutt"
---

Dark Crystal started as a project to backup secrets over the peer-to-peer social network and application platform [Secure scuttlebutt](https://scuttlebutt.nz/).

![Dark crystal on SSB](/assets/dark-crystal-3.gif)
> *Here we see a short demo of entering data and selecting custodians.*

It is available inside the Scuttlebutt client [patchbay](https://github.com/ssbc/patchbay/) as well as a [stand-alone application](https://github.com/blockades/dark-crystal-standalone) for existing scuttlebutt users.

It can be used for backing up the private key to your scuttlebutt account. 
Scuttlebutt's distributed nature means your content itself is automatically preserved by your peers, so your private key is really all you need in order to completely restore your account.

![History of a secret](/assets/secret-history-screenshot.png)

> *This screenshot shows the history of a secret; who the shards were sent to and buttons to request them back.*

This original dark-crystal implementation was designed to be a generic tool for backing up any key or secret, so the peer is able to enter the secret itself.

This has the advantage that it can be used with any secret, for example, you could paste in your GPG key, but we found that the process of copying and pasting keys can be quite cumbersome, error-prone and has security issues. 

This inspired us to move away from creating a generic tool for backing up secrets, and instead create a framework which can be integrated into applications which handle sensitive data.

## Learnings from the original implementation

- [Peer testing and usability assessment](https://gitlab.com/dark-crystal-javascript/research/blob/master/dark_crystal-report_peer_testing_and_usability_assessment.md)
- [UX Report from UnderExposed Design Residency with Ura Design - Part1](https://gitlab.com/dark-crystal-javascript/research/blob/master/underexposed1.md)
- [UX Report from UnderExposed Design Residency with Ura Design - Part 2](https://gitlab.com/dark-crystal-javascript/research/blob/master/underexposed2.md)
