---
title: "Dark Crystal Web3"
toc: false
---

![cave pixel art](/assets/cavepixelart.jpg)

Dark Crystal Web3 is a tool for backing up and recovering secrets using a set of trusted contacts.

Unlike other implementations of Dark Crystal, the recovery partners do not need to look after their shares.  The shares are encrypted using a keypair derived from an Ethereum signature, and published publicly using a smart contract. This means that provided the recovery partners have an Ethereum account, they don't need to install any software or look after any additional secret data. Although the encrypted shares are published openly, the identities of the recovery partners remain secret.

- The alpha release is available at [web3.darkcrystal.pw](https://web3.darkcrystal.pw)
- Source code at [gitlab.com/dark-crystal-web3](https://gitlab.com/dark-crystal-web3)


![Open treasure chest](/assets/backupOpen.png)

As well as using the dAPP linked above, the secret owner can create and recover backups using an offline-first client.

## How to use it

In order to backup a secret, you first need to choose some trusted contacts and ask them to generate a public key and send it to you.

They can do this by visiting [web3.darkcrystal.pw/generate](https://web3.darkcrystal.pw/generate) and following the instructions. They will need a browser-extension based Ethereum wallet such as [Metamask](https://metamask.io).

Once you have the public keys of all your chosen contacts, you can make a backup. This can be done in one of three ways:

- Using the dAPP: [web3.darkcrystal.pw/backup](https://web3.darkcrystal.pw/backup)
- Using the offline-first CLI (installation instructions below)
- Using an offline-first version of the web interface served from the CLI.

You will need to choose an identifier or 'lookup key' when making the backup. This will be kept private. We recommend you use your Ethereum address, but you can choose something else as long as you can remember it during recovery.

If using the dAPP, your encrypted backup will then be published. If using the offline-first client, you will be given a link to the dAPP containing the encrypted backup payload. Your backup will be published when this link is opened. The link is also available as a QR code allowing you to transfer it from an air-gapped device.

<!-- TODO explain threshold -->

## Recovery

In order to recover a backup you will need to give the identifier to your trusted contacts and ask them to decrypt their share and send it to you. They can do this here: [web3.darkcrystal.pw/decrypt](https://web3.darkcrystal.pw/decrypt).

Once you have enough shares, you can recover the secret. Again, you can either do this using the ['recover' option in the dapp](https://web3.darkcrystal.pw/recover) or using the offline-first client.

## Installation of the offline-first client

If you have cargo, you can install with:

```
cargo install dark-crystal-web3
```

Then run `dark-crystal-web3` to see usage instructions for the command line client.

### Installing without cargo

If you don't have cargo, you can download the release binary for linux from [darkcrystal.pw/assets/dark-crystal-web3](https://darkcrystal.pw/assets/dark-crystal-web3) and make it executable with the following command:

```
chmod a+x dark-crystal-web3
```

You may also want to move it to a path for binaries, for example:

```
mv dark-crystal-web3 ~/.local/bin
```

If you want to check the integrity of the release binary you can do:

```
sha256sum dark-crystal-web3
```

and the output should match:

`6feb6769825749a3beff6dfaeb19186ed494fe334cd894c8218c982675d00884`

### Installing the offline-first web ui

If you want to use the offline-first version of the web-ui, you can install it with:

```
git clone --depth 1 https://gitlab.com/dark-crystal-web3/dark-crystal-web3-backup-prebuilt-web-ui ~/.local/share/dark-crystal-web3
```

or on macOS:

```
git clone --depth 1 https://gitlab.com/dark-crystal-web3/dark-crystal-web3-backup-prebuilt-web-ui $HOME/Library/Application\ Support/dark-crystal-web3
```

And start it with:

```
dark-crystal-web3 serve
```
