---
title: "Projects using Dark Crystal"
---

![Open treasure chest](/assets/backupOpen.png)

[Dark Crystal Web3]({{<ref "web3">}}) is a tool for backing up and recovering secrets using a smart contract.

[![briar dark crystal](/assets/briar-dark-crystal.png)]({{< ref "post/briar-dc-meetup" >}})

Social backups are currently being integrated into Briar. See our [blog posts]({{< ref "post/briar-dc-meetup">}}) and [Project case report](/assets/briar-case-project-report.pdf) for details.

[![Hermie Patchbay](/assets/hermiepatchbay.svg)]({{< ref "scuttlebutt-application.md">}})

Social backups are [integrated into the Patchbay Secure Scuttlebutt client]({{< ref "scuttlebutt-application.md" >}})
