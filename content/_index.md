---
date: "2017-06-26T18:27:58+01:00"
title: "Dark Crystal"
---

![dark crystal logo](/assets/dark-crystal-icon_200x200.png)

### Backup your secrets using the trust in your social fabric

Dark Crystal is a set of protocols, libraries, techniques and guidelines for secure management of sensitive data such as cryptographic keys. 

The idea is to make key management easy by emphasising trust between peers rather than individual responsibility.

It is a toolkit for developers who want to bring secure and easy to use key management techniques to their project.  It is particularly well suited for decentralised systems where authentication relies on keys stored on a peer's device.

{{< ticks >}}
- **Transport agnostic** - Key backup and recovery mechanisms that work with your existing transport mechanism.
- **Flexible** - Pick features which meet the needs of your individual project.
- **Easy to use** - Includes template designs for making intuitive interfaces.
- **Empowering for peers** - Peers keys are secured by trust in their social network.
{{< /ticks >}}

There is a [reference implementation in Java](https://gitlab.com/dark-crystal-java), a [Javascript implementation](https://gitlab.com/dark-crystal-javascript) and a work-in-progress [Rust implementation](https://gitlab.com/dark-crystal-rust).

Dark Crystal is 100% open source, licensed [Lesser GPL3](https://www.gnu.org/licenses/lgpl-3.0.en.html).  You can use [our modules](https://gitlab.com/dark-crystal-java) in your projects, or just take inspiration from them to implement something similar. Every project has different needs, so we are trying to establish good patterns rather than build a generic tool which everybody should use.

The Dark Crystal Key Backup protocol and Java implementation have been independently audited by [Include Security](https://includesecurity.com/), supported by the [Open Technology Fund's Red Team Lab](https://www.opentech.fund/labs/red-team-lab). [Read the security review report](/assets/2021_Q3_OTF_and_Magma_Collective_-_Dark_Crystal_Protocol_-_Report.pdf)

The reference implementation uses conventions from the [libsodium](https://download.libsodium.org/doc/) crypto library and [Daan Sprenkle's secret sharing library](https://github.com/dsprenkels/sss), both are written in C with bindings to many popular languages.

For a detailed discussion of our protocol and possible further use-cases, see our report: ['The Social and Technical Applications of Threshold Based Secret-Sharing in an Internet Freedom Context'](/assets/dark-crystal-final-report.pdf).
