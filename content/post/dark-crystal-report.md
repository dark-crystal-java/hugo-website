---
title: "Dark Crystal Report"
date: 2022-03-15T10:47:20+01:00
image: "/assets/final-report-preview.png"
toc: false
draft: false
---

We have published our report ['The Social and Technical Applications of Threshold Based Secret-Sharing in an Internet Freedom Context'](/assets/dark-crystal-final-report.pdf).

In the report we discuss our protocol and our work with Briar, and go on to discuss some wider use-cases of threshold-based consensus, looking in particular at group governance.

We have also finished our current development phase on our two Briar features, following the last round of user testing which was hosted in Indonesia by [SAFEnet](https://safenet.or.id). You can see some screenshots of the UI improvements we made to [Social Backup](https://gitlab.com/dark-crystal-java/briar-social-backup-feature-testing-walkthrough/-/blob/master/possible-improvements.md) and [Remote Wipe](https://gitlab.com/dark-crystal-java/briar-remote-wipe-feature-testing-walkthrough/-/blob/master/possible-improvements.md).

We have also started work on a [Rust implementation of our Key Backup protocol](https://gitlab.com/dark-crystal-rust), with two crates published: [Secret Sharing](https://crates.io/crates/dark-crystal-secret-sharing-rust) and [Key Backup](https://crates.io/crates/dark-crystal-key-backup-rust).
